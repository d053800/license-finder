# frozen_string_literal: true

module License
  module Management
    VERSION = '4.0.0'
  end
end
