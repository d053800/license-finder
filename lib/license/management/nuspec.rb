# frozen_string_literal: true

module License
  module Management
    class Nuspec
      attr_reader :xml

      def initialize(xml)
        @xml = REXML::Document.new(xml)
      end

      def licenses
        licenses = REXML::XPath.match(xml, "//package/metadata/license[@type='expression']").map(&:get_text).map(&:to_s)
        return licenses if licenses.any?

        REXML::XPath.match(xml, '//package/metadata/licenseUrl').map(&:get_text).map(&:to_s)
      end
    end
  end
end
