# frozen_string_literal: true

name "asdf_gradle"
default_version "6.5.1"

source url: "https://services.gradle.org/distributions/gradle-#{version}-bin.zip"
relative_path "gradle-#{version}"

version "6.5.1" do
  source sha256: "50a7d30529fa939721fe9268a0205142f3f2302bcac5fb45b27a3902e58db54a"
end

build do
  mkdir install_dir
  copy "#{project_dir}/**", "#{install_dir}/"
end
