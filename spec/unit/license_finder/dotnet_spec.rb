# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LicenseFinder::Dotnet do
  let(:package_manager) { described_class.new(options) }
  let(:options) { { ignored_groups: [], project_path: project.project_path } }
  let(:project) { ProjectHelper.new }

  before do
    project.mount(dir: project_fixture)
  end

  after do
    project.cleanup
  end

  describe "#dotnet_version" do
    subject { package_manager.dotnet_version }

    context "when the version of dotnet is specified in a .tool-versions file" do
      let(:project_fixture) { fixture_file('dotnet/nuget-csproj') }

      before do
        project.add_file('.tool-versions', 'dotnet-core 3.1.301')
      end

      specify { expect(subject).to eql('3.1.301') }
    end

    context "when a nodejs version is not specified" do
      let(:project_fixture) { fixture_file('dotnet/nuget-csproj') }

      specify { expect(subject).to eql('3.1.302') }
    end
  end
end
